FROM node:latest

# ENV VARS
ARG NPM_TOKEN

# SETUP
WORKDIR /home/node/app
SHELL ["/bin/bash", "-c"]

# DEPENDENCIES
COPY ./package*.json ./
COPY ./index.js ./
RUN ls -al
RUN npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' $NPM_TOKEN
RUN npm config set @groupe-bananes:registry https://gitlab.com/api/v4/packages/npm/
RUN cat ~/.npmrc
RUN echo $NPM_TOKEN
RUN npm install

# RUN
CMD node index.js
