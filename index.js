// Setup for testing with different groups/packages
const GROUP_NAME = '@groupe-bananes';
const PACKAGE_NAME = 'my-package';
const PACKAGE_NAME_IN_REGISTRY = [GROUP_NAME, PACKAGE_NAME].join('/');

// Make logging more friendly
function log(...messages) {
	messages.forEach(msg => console.log(msg));
}

// Start!
log(
	'hello',
	`let's take a look and see if ${PACKAGE_NAME_IN_REGISTRY} is require-able`
);

// Here we will try to require the package, and then try to access its own properties
// to verify that what we got is what we expected. If the require call fails, then we
// know there is an issue. If calling some method on the required object fails, then
// that's not really our concern for this test.
try {
	const package = require(PACKAGE_NAME_IN_REGISTRY);
	log(
		'if you got here, then it worked, and the package looks like this:',
		package,
		'let\'s see what the package offers...',
	);

	for (let prop of Object.getOwnPropertyNames(package)) {
		if (typeof package[prop] === 'function') {
			log(
				`running method with name ${prop}`,
				'----'
			);
			try {
				package[prop]();
			} catch (methodCallError) {
				log(
					`${method} threw`,
					methodCallError,
					'... continuing, as the error is likely due to using the method out of context. This is just a test...'
				);
			}
			log(
				'----',
				`ran method with name ${prop}`
			);
		} else {
			log(
				`package includes a property called ${prop} which has the following value:`,
				prop
			);
		}
	}

	// Keep open for testing. Uncomment to kill the docker image when this succeeds.
	// process.exit(0);
} catch (requireCallError) {
	log(
		'no dice...',
		`could not require ${PACKAGE_NAME_IN_REGISTRY} due to following error:`,
		requireCallError);
	throw requireCallError;
}
