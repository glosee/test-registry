# GitLab NPM Registry Sandbox

This repo was created to fully outline all that is required for different users of the GitLab NPM registry. It also sets out to help debug some ongoing access issues we've faced when trying to install packages that were published to the GitLab NPM Registry.

## Project contents

This project consists of a package.json file with a single dependency that's been published to GitLab, a simple program that tries to load said dependency, and a Dockerfile that creates a VM to run said program in a completely clean sandboxed environment.

The package we are testing with is called `@groupe-bananes/my-package`, which can be found here: https://gitlab.com/groupe-bananes/my-package.

## Why did we create this?

There are a couple known issues that we've run into when trying to use the GitLab registry. What I've been finding as I dig into this is that the documentation from GitLab is often incomplete or unclear, leading me to find that we don't fully understand all the pieces needed to use the GitLab registry whole-heartedly.

Specifically I am trying to get to the bottom of understanding everything that is required for the personas detailed below.

The goal of this repository is to create a contrived repository to demonstrate all the known issues we've stumbled upon, which includes:

* Not being able to install packages locally, despite having a GitLab access token
* Not being able to install packages in a CI pipeline, despite following directions from GitLab's documentation using `CI_JOB_TOKEN`
* Not being able to install packages in a Netlify deploy, despite following directions from GitLab's documentation for using Deploy Tokens for the given Project (and / or Group) that the package has been published under

## Personas / use cases

### Package developer

A package developer is someone who is contributing to a package that is intended to be (or is already) published in the GitLab registry.

### Package consumer (developer on another team)

A package consumer is a developer who may not be contributing to a package, but who wants to use a package that has been published in the GitLab registry on their machine for local development.

### CI Pipelines in GitLab

CI Pipelines in GitLab may need to run `npm install` to install dependencies directly in a runner (and in most cases cache those dependencies for subsequent jobs) for running linters, tests, build scripts, etc. Projects that depend on packages in the GitLab registry will need access. Due to how GitLab bundles packages under groups, there are technically two possibilities here:
- A pipeline running in a project under _the same group_ where a package has been published (eg. the project `group-a/my-project` trying to use a package published at `@group-a/my-package`)
- A pipeline running in a project under _a different group_ than where a package has been published (eg. the project `group-b/my-project` trying to use a package published at `@group-a/my-package`).
	> _NOTE: There is a known issue for this usecase that is being tracked by GitLab support, see here: https://gitlab.com/gitlab-org/gitlab/-/issues/213723_

The GitLab docs suggest that you can use the environment variable `CI_JOB_TOKEN` to authenticate with the GitLab registry, but there are some nuances with using this token. Specifically the permissions given to pipelines in GitLab are inherited from the user who triggered the pipeline. So if `UserA` pushes a commit to a branch that has an open MR, then the `CI_JOB_TOKEN` for that pipeline inherits all the permissions of `UserA`. If a pipeline was triggered by a schedule, or some other mechanism, then the `CI_JOB_TOKEN` inherits a global permission set that includes the ability to read packages (see the aforementioned GitLab support issue for more details https://gitlab.com/gitlab-org/gitlab/-/issues/213723).

### Deploy builds in Netlify

We have a number of projects that use Netlify for builds instead of GitLab. Similar to the CI Pipelines in GitLab, a project's build will run `npm install`. When a project depends on a package from the GitLab registry, then it will need authorization to access those packages.

The GitLab docs suggest that you can use a `Deploy Token` to grant read access to a group/project's packages.

## How to use this repo

Here are some basic steps for running this example.

### Requirements

- Docker

### The NPM_TOKEN variable

The `Dockerfile` acts as a sandbox environment for demonstrating the access issue. The `Dockerfile` expects (and exposes in the image it builds) an environment variable called `NPM_TOKEN` to exist in the environment in which it is run. To demonstrate different scenarios you can use different values for this variable - either a personal access token, or a deploy token for a group and/or project. Steps to get either of these tokens are below.

#### Getting a personal access token

Each user in GitLab has the ability to generate personal access tokens, which will inherit all permissions/roles for that user. To get your personal access token:

- Click on your user avatar in the top right corner, and click on Settings in the dropdown
- Click on Access Tokens in the right-hand sidebar
- Fill out the form to create a new access token, ensuring that you add either the `api` or the `read_api` scope, which is required to access the package registry
- Copy the generated token and save it somewhere on your machine (GitLab will only expose the token once here, so make sure you capture it before navigating away from the page)

#### Getting a deploy token for a group

Each group and project has the concept of a Deploy Token, which is designed to be used for this type of thing. It is supposed to have access to the package registry for a given group/project, but we have found that this does not always work.

> *NOTE:* only Maintainers and Owners on groups can access this. If you need a Deploy Token for a Group or Project where you are not in one of these roles, then you will have to reach out to the owner of that Group or Project.

To get a deploy token for a group/project that you own:

- Go to the group/project in GitLab, eg. https://gitlab.com/groupe-bananes/
- Go to Settings > Repository
- Expand the "Deploy Tokens" accordion
- Follow the steps / fill out the form to create a new Deploy Token (ensuring you add the `read_repository` scope (which is the only available scope to select at the time of writing this))
- Copy the deploy token and save it somewhere on your machine (GitLab will only expose the token once here, so make sure you capture it before navigating away from the page)

### Building and running the example

- Ensure that you have `NPM_TOKEN` exposed in your envionrment

```
export $NPM_TOKEN=my_access_token
```

- Build the `Docker` image, exposing the `NPM_TOKEN` environment variable to the `Docker` context, and where `-t` is an arbitrary tag name you'll use later

```
docker build -t my_tag_name --build-arg NPM_TOKEN .
```

_At this point, the build may or may not fail, depending on the token you used._

- Run a bash shell in the `Docker` image

```
docker run --rm --init -it my_tag_name /bin/bash
```

_This will allow you to create a bash session inside the image, which you can use for further testing, eg. trying to install more packages form the registry, or changing the token value to try again._
